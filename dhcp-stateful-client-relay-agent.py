#!/opt/local/bin/python2.7
from pydhcplib.dhcp_packet import *
from pydhcplib.dhcp_network import *
import argparse
import atexit


class Client(DhcpClient):
    def __init__(self, options):

        print options
        DhcpClient.__init__(self,options["listen_address"],
                            options["client_listen_port"],
                            options["server_listen_port"])

    def HandleDhcpOffer(self, packet):
        chaddr=hwmac(packet.GetOption('chaddr')[:6])
        print "OFR", chaddr.str() 
        packet.SetOption('op',[1])
        packet.SetOption('dhcp_message_type',[3]) # DHCPREQUEST
        packet.SetOption('ciaddr',packet.GetOption('yiaddr')) # DHCPREQUEST
        self.SendDhcpPacketTo(packet,server,67)


    def HandleDhcpAck(self, packet):
        chaddr=hwmac(packet.GetOption('chaddr')[:6])
        print "ACK", chaddr.str() 
        packet.SetOption('op',[1])
        packet.SetOption('dhcp_message_type',[7]) # DHCPREQUEST
        packet.SetOption('ciaddr',packet.GetOption('yiaddr')) # DHCPREQUEST
        self.SendDhcpPacketTo(packet,server,67)
    def HandleDhcpNack(self, packet):
        print packet.str()        

parser = argparse.ArgumentParser(
        description='''\
Statelessly load test a dhcp server by acting as a relay agent. 

This will do very bad things to your DHCP server.  You have been warned. 

This software generates a stream at maximum rate of DHCPDISCOVER requests 
from random MAC addresses aimed at a server, while claiming to be a relay 
agent.  The requests this generates are real, the clients aren't however.
''') 

parser.add_argument('--server','-s', metavar='server', nargs=1, required=True)
parser.add_argument('--giaddr','--relayip','-r', metavar='RelayAgentIP', nargs=1, required=True)

args = parser.parse_args()

print args

server = args.server[0]
giaddr = args.giaddr[0]


netopt = {'client_listen_port':67,
           'server_listen_port':67,
           'listen_address':giaddr
            }


client = Client(netopt)
client.BindToAddress()

while True :
    client.GetNextDhcpPacket()



