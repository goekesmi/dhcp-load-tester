#!/opt/local/bin/python2.7

from random import Random
from pydhcplib.dhcp_packet      import DhcpPacket
from pydhcplib.type_ipv4        import ipv4
from pydhcplib.type_hw_addr     import hwmac
from pydhcplib.dhcp_network     import DhcpClient
import argparse
import time
import signal
import sys

#Initialize the random number generator
r = Random()
r.seed()


# random mac address
def genmac():
        i = []
        for z in xrange(3):
                i.append(r.randint(0,255))
        # vendor prefix FE:E3:23 is currently unassigned, and should never be
        # assigned.
        return 'fe:e3:23:' + ':'.join(map(lambda x: "%02x"%x,i))

def genxid():
        decxid = r.randint(0,0xffffffff)
        xid = []
        for i in xrange(4):
                xid.insert(0, decxid & 0xff)
                decxid = decxid >> 8
        return xid

def AssembleRelayAgentDiscover(
                        xid=None,                       # Transaction ID
                        chaddr='00:00:00:00:00:00',     # Client HW address
                        giaddr="0.0.0.0",               # Gateway IP address, 
                                                        # AKA Relay agent
                        ):

        ciaddr="0.0.0.0"                # Client IP address, default = 'I don't know'

        req = DhcpPacket()
        req.SetOption('op',[1])         # Op code, 1 = BOOTREQUEST
        req.SetOption('htype',[1])      # Hardware type, 1 = Ethernet
        req.SetOption('hlen',[6])       # Hardware address lenght, 6 bytes in a MAC
        req.SetOption('hops',[0])       # network hops, 0 default, optional usage
        if not xid:
                xid = genxid()
        req.SetOption('xid',xid)        
        req.SetOption('giaddr',ipv4(giaddr).list())
        req.SetOption('chaddr',hwmac(chaddr).list() + [0] * 10)
        req.SetOption('ciaddr',ipv4(ciaddr).list())
        req.SetOption('dhcp_message_type',[1]) # DHCPDISCOVER
        return req

parser = argparse.ArgumentParser(
        description='''\
Statelessly load test a dhcp server by acting as a relay agent. 

This will do very bad things to your DHCP server.  You have been warned. 

This software generates a stream at maximum rate of DHCPDISCOVER requests 
from random MAC addresses aimed at a server, while claiming to be a relay 
agent.  The requests this generates are real, the clients aren't however.
''') 

parser.add_argument('--server','-s', metavar='server', nargs=1, required=True)
parser.add_argument('--giaddr','--relayip','-r', metavar='RelayAgentIP', nargs=1, required=True)
parser.add_argument('--count','-c', metavar='total packets', default = [-1], nargs=1, type=int )
parser.add_argument('--burst','-b', metavar='packets', default = [100] , nargs=1, type=int )
parser.add_argument('--pause','-p', metavar='seconds', default = [1], nargs=1, type=float )
parser.add_argument('--discoverlog','-l', metavar='logfile', nargs=1 )
parser.add_argument('--pps','-P', metavar='packets', nargs=1, type=int )

args = parser.parse_args()

print args

server = args.server[0]
giaddr = args.giaddr[0]

count = args.count[0]
burst = args.burst[0]

try:
        logfile = open(args.discoverlog[0], 'w')
except TypeError:
	logfile = None
        

while count != 0:
	starttime = time.time()
        chaddr = genmac()
        discover = AssembleRelayAgentDiscover( chaddr=chaddr ,giaddr=giaddr)
        client = DhcpClient(client_listen_port=67, server_listen_port=67)
        client.SendDhcpPacketTo(discover,server,67)
	try:
                logfile.write(chaddr)
                logfile.write("\n")
        except:
                None
        count = int (count) - 1

        endtime = time.time()
	try:
		if (( (1.0 / args.pps[0])- (endtime - starttime) > 0 )):
                     time.sleep( ( (1.0 / args.pps[0])- (endtime - starttime) ) )
        except KeyboardInterrupt :
                raise
	except:
                burst = int (burst) - 1
                if (burst == 0):
                        burst = args.burst[0]
                        time.sleep( int( args.pause[0]) )



