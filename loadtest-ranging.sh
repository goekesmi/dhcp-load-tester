#!/opt/local/bin/bash

SERVER_IP="35.8.2.89"
RELAY_IP="35.8.0.38"
COUNT=1000

for pps in 50; do
        mkdir P$pps;
        cd P$pps;
        ../dhcp-stateful-client-relay-agent.py -s $SERVER_IP -r $RELAY_IP >> clientlog &
        FOOPID=$!
        ../dhcp-discover-relay-agent.py -s $SERVER_IP -r $RELAY_IP -c $COUNT -P $pps -l discoverlog;
        sleep 3;
        kill $FOOPID;
        kill -9 $FOOPID;
        cd ..;
done

