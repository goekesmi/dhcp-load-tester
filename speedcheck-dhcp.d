BEGIN 
{
discover_t = timestamp;
client_t  = timestamp;
}

syscall::sendto:entry
/execname == "dhcp-discover-re"/
{
	@discover_interval = quantize(timestamp - discover_t);
	discover_t = timestamp;
}

syscall::sendto:entry
/execname == "dhcp-stateful-cl"/
{
	@client_interval = quantize(timestamp - client_t );
	client_t = timestamp;
}
